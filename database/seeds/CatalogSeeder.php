<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class CatalogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1,20) as $index) {
            DB::table('catalog')->insert([
                'name' => $faker->word,
                'qty' => $faker->numberBetween(1,100),
                'price' => $faker->randomFloat(2, 1, 1500),
                'description' => $faker->realText(100),
            ]);
        }    }
}
