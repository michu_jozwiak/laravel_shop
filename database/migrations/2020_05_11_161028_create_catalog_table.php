<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogTable extends Migration
{

    public function up()
    {
        Schema::create('catalog', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->integer('qty');
            $table->float('price');
            $table->text('description');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('catalog');
    }
}
