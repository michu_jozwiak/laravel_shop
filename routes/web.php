<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CatalogController@index');

Route::get('cart/', 'CartController@index');
Route::post('addToCart/', 'CartController@addToCart');
Route::post('removeFromCart/', 'CartController@removeFromCart');

Route::get('checkout/', 'CheckoutController@index');
Route::post('placeOrder/', 'CheckoutController@placeOrder');
