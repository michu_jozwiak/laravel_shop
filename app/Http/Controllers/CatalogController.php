<?php
namespace App\Http\Controllers;

use App\Catalog;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Laravel\Lumen\Routing\Controller as BaseController;

class CatalogController extends Controller
{
    private const PER_PAGE = 4;

    public function index(Request $request)
    {
        if (!$request->session()->get('user')) {
            $request->session()->put('user', Str::random(9));
        }

        $catalog = Catalog::paginate(static::PER_PAGE);
        return view('catalog')->with(
            [
                'catalog' => $catalog
            ]
        );
    }
}
