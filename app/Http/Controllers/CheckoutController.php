<?php
namespace App\Http\Controllers;

use App\Cart;
use App\Catalog;
use App\Order;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    public function index(Request $request)
    {
        return view('checkout');
    }

    public function placeOrder(Request $request)
    {
        $request->validate(
            [
                'firstName' => 'required|max:255',
                'lastName' => 'required|max:255',
                'address' => 'required|max: 500',
                'city' => 'required|max: 255',
                'zip' => 'required',
                'email' => 'required|regex:/^.+@.+$/i',
                'phone' => 'required|regex:/[0-9]{9}/',
            ]
        );
        $order = new Order();
        $order->customer_first_name = $request->post('firstName');
        $order->customer_last_name = $request->post('lastName');
        $order->address = $request->post('address');
        $order->city = $request->post('city');
        $order->zip = $request->post('zip');
        $order->email = $request->post('email');
        $order->phone = $request->post('phone');
        $order->nip = $request->post('nip') ?? '';

        $cart = Cart::where('cart_id', $request->session()->get('user'))
            ->where('closed', 0)
            ;
        $order->cart_items_ids = $cart->pluck('id')->toJson();

        $total = 0;
        foreach ($cart->get() as $cartItem) {
            $total += $cartItem->product->price * $cartItem->qty;
        }
        $order->total = $total;

        try {
            $order->save();

            foreach ($cart->get() as $cartItem) {
                $product = Catalog::where('id', $cartItem->product->id)->first();
                $product->update(['qty' => $product->qty - $cartItem->qty]);
            }

            mail($request->post('email'), 'Order: ' . $order->id, $cart->get()->toJson());
            $cart->update(['closed' => 1]);
        } catch (\Exception $e) {
            return back()->withErrors(['error' => $e->getMessage()]);
        }

        $request->session()->flash('success', 'Order placed');
        return back();
    }
}
