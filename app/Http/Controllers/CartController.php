<?php
namespace App\Http\Controllers;

use App\Cart;
use App\Catalog;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function index(Request $request)
    {
        $cart = Cart::where('cart_id', $request->session()->get('user'))
            ->where('closed', 0)
            ->get();

        $total = 0;
        foreach ($cart as $item) {
            $total += $item->product->price * $item->qty;
        }
        return view('cart')->with(
            [
                'cart' => $cart,
                'total' => $total
            ]
        );
    }

    public function addToCart(Request $request)
    {
        $productId = $request->post('product_id');
        $qty = $request->post('qty');

        /** @var Catalog $product */
        $product = Catalog::where('id', $productId);
        if (!$product->count() || $qty > $product->first()->qty) {
            return back()->withErrors(['error' => 'Problem with adding product']);
        }

        try {
            $cart = Cart::where('cart_id', $request->session()->get('user'))
                ->where('product_id', $productId)
                ->where('closed', 0)
                ->first();

            if (!$cart) {
                $cart = new Cart();
                $cart->cart_id = $request->session()->get('user');
                $cart->product_id = $productId;
                $cart->closed = 0;
                $cart->qty = 0;
            }

            $cart->qty += $qty;
            $cart->save();
        } catch (\Exception $e) {
            return back()->withErrors(['error' => $e->getMessage()]);
        }

        $request->session()->flash('success', 'Added to cart');
        return back();
    }

    public function removeFromCart(Request $request)
    {
        $cartItemId = $request->post('cartItemId');

        try {
            $cartItem = Cart::where('id', $cartItemId);
            $cartItem->delete();
        } catch (\Exception $e) {
            return back()->withInput(['error' => $e->getMessage()]);
        }

        return back()->withInput(['success' => 'Remove from cart']);
    }
}
