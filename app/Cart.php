<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'cart';

    protected $fillable = ['cart_id', 'product_id', 'qty'];

    public function product()
    {
        return $this->belongsTo(Catalog::class, 'product_id');
    }
}
