@extends('layouts.app')

@section('title', 'Cart')

@section('content')
    <div class="card shopping-cart">
        <div class="card-body">
            @foreach($cart as $cartItem)
            <div class="row">
                <div class="col-12 col-sm-12 col-md-2 text-center">
                    <img class="img-responsive" src="http://placehold.it/120x80" alt="prewiew" width="120" height="80">
                </div>
                <div class="col-12 text-sm-center col-sm-12 text-md-left col-md-6">
                    <h4 class="product-name"><strong>{{ $cartItem->product->name }}</strong></h4>
                    <h4>
                        <small>{{ $cartItem->product->description }}</small>
                    </h4>
                </div>
                <div class="col-12 col-sm-12 text-sm-center col-md-4 text-md-right row">
                    <div class="col-3 col-sm-3 col-md-6 text-md-right" style="padding-top: 5px">
                        <h6><strong>{{ money($cartItem->product->price, 'PLN', true) }}</strong></h6>
                    </div>
                    <div class="col-4 col-sm-4 col-md-4">
                        <div class="quantity">
                            <input type="number" step="1" max="99" min="1" title="Qty" class="qty"
                                   size="4" value="{{ $cartItem->qty }}" readonly>
                        </div>
                    </div>
                    <div class="col-2 col-sm-2 col-md-2 text-right">
                        <form action="/removeFromCart" method="post">
                            @csrf
                            <input name="cartItemId" class="text-hide" type="text" value="{{ $cartItem->id }}">
                            <button type="submit" class="btn btn-outline-danger btn-xs">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <hr>
            @endforeach
        </div>
        <div class="card-footer">
            <div class="pull-right" style="margin: 10px">
                <a href="/checkout" class="btn btn-success pull-right">Checkout</a>
                <div class="pull-right" style="margin: 5px">
                    Total price: <b>{{ money($total, 'PLN', true) }}</b>
                </div>
            </div>
        </div>
    </div>
@endsection
