@extends('layouts.app')

@section('title', 'Catalog')

@section('content')
    <div class="card-deck text-center">
        @foreach($catalog as $product)
            <div class="card mb-4 shadow-sm">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">{{ $product->name }}</h4>
                </div>
                <div class="card-body">
                    <img class="bd-placeholder-img card-img-top" src="https://via.placeholder.com/400" alt="image">
                    <h1 class="card-title pricing-card-title">{{ money($product->price, 'PLN', true) }}</h1>
                    <div class="list-unstyled mt-3 mb-4">
                        {{ $product->description }}
                    </div>
                    @if($product->qty > 0)
                        <form method="POST" action="/addToCart">
                            @csrf
                            <input class="text-hide" type="text" name="product_id" value="{{ $product->id }}">
                            <div class="form-group" mb-2>
                                <label for="exampleInputEmail1">Qty</label>
                                <input type="number" class="form-control" name="qty" id="qty" step="1" max="{{ $product->qty }}" size="4" aria-describedby="Qty" placeholder="Quantity">
                            </div>
                            <button type="submit" class="btn btn-lg btn-block btn-outline-primary">@lang('Add to cart')</button>
                        </form>
                    @else
                        <div class="list-unstyled mt-3 mb-4 alert-danger">
                            @lang('Product out of stock')
                        </div>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
    <div class="pagination justify-content-center">
        {{ $catalog->links() }}
    </div>
@endsection
