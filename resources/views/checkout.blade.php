@extends('layouts.app')

@section('title', 'Checkout')

@section('content')
<form action="/placeOrder" method="post">
    @csrf
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="firstName">@lang('Name')</label>
            <input type="text" class="form-control" id="firstName"  name="firstName" placeholder="First name">
        </div>
        <div class="form-group col-md-6">
            <label for="lastName">@lang('Last Name')</label>
            <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last name">
        </div>
    </div>
    <div class="form-group">
        <label for="address">@lang('Address')</label>
        <input type="text" class="form-control" id="address" name="address" placeholder="@lang('1234 Main St')">
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="city">@lang('City')</label>
            <input type="text" class="form-control" id="city" name="city">
        </div>
        <div class="form-group col-md-6">
            <label for="zip">@lang('Zip')</label>
            <input type="text" class="form-control" id="zip" name="zip">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="phone">@lang('Phone')</label>
            <input type="text" class="form-control" id="phone" name="phone" placeholder="123 456 789">
        </div>
        <div class="form-group col-md-6">
            <label for="email">@lang('Email')</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="xxxx@xxxx.com">
        </div>
    </div>
    <div class="form-group">
        <label for="nip">@lang('NIP')</label>
        <input type="text" class="form-control" id="nip" name="nip">
    </div>
    <button type="submit" class="btn btn-primary">@lang('Place order')</button>
</form>
@endsection
